const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync');

const sassFiles = ['**/*.scss', '!node_modules/**/*.scss'];

gulp.task('styles', () => {
  gulp.src(sassFiles)
      .pipe(sourcemaps.init())
      .pipe(sass().on('error', sass.logError))
      .pipe(sourcemaps.write('./', {includeContent: false, sourceRoot: './'}))
      .pipe(gulp.dest('./'))
      .pipe(browserSync.reload({stream:true}));
});

gulp.task('browser-sync', () => {
  browserSync({
    server: {
      baseDir: "./"
    }
  });
});

gulp.task('default', ['styles', 'browser-sync'], () =>{
  gulp.watch(sassFiles, ['styles']);
});