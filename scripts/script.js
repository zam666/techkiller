// MOBILE MENU

$(document).ready(function(){
	$('.mobile-menu-icon').click(function(){
		$(this).toggleClass('open');
		$('.mobile-menu').toggleClass('open');
	});
});

// OWL SLIDER

$(document).ready(function() {

  // OWL SLIDER

  $(".owl-carousel").owlCarousel({
  loop: true,
  items: 1,
  margin: 0,
  dots: true,
  nav: true,
  navText: ["<img class='arrow-left' src='../images/arrow-left.png'>",
            "<img class='arrow-right' src='../images/arrow-right.png'>"]
  })

});

$('.owl-dot').click(function () {
    owl.trigger('to.owl.carousel', [$(this).index(), 300]);
});

// SORT

$(document).ready(function(){
	$('.button-sort').click(function(){
		$('.button-sort-list').toggleClass('open');
	});
});

// TO TOP

$(".button-to-top").on( "click", function(event) {
  event.preventDefault();
  $('html, body').animate({scrollTop: 0}, 700);
});


